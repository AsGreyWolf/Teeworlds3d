#include "Model.h"

Model::Model() {}
Model::Model(const Model &second) {}
Model::~Model() {}
void Model::Render() {}
void Model::Enable() { enabled = true; }
void Model::Disable() { enabled = false; }
